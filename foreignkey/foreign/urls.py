from django.conf.urls import url
from django.views.decorators.csrf import csrf_exempt
from foreign.views import DataInputAndOutput

urlpatterns = [
	url(r'^foreign/inputdata/$', csrf_exempt(DataInputAndOutput.as_view())),
]