from __future__ import unicode_literals
from django.db import models

# Create your models here.

#creating the item model
class FoodItem(models.Model):
	item_name = models.CharField(max_length=255)
	item_price = models.CharField(max_length=255)
	item_category = models.CharField(max_length=255)

	def __unicode__(self):
		return (str(self.item_name) 
					+' ' 
					+str(self.item_price) 
					+' ' 
					+str(self.item_category))

#creating FoodOreder and here FoodItem is a foreign Key 
class FoodOrder(models.Model):
	item_name = models.ForeignKey(FoodItem, on_delete=models.CASCADE)
	order_ammount = models.CharField(max_length=255)

	def __unicode__(self):
		return (str(self.item_name) 
					+' ' 
					+str(self.order_ammount))