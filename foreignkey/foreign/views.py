from django.shortcuts import render
from django.http import HttpResponse
from django.core import serializers
from django.http import QueryDict
from django.views.generic import View
import json
from foreign.models import FoodItem, FoodOrder
# Create your views here.

class DataInputAndOutput(View):
	"""post, put, get method applied in foreign key table"""

	def get(self, request, *args, **kargs):
		"""Getting Food Item value using all, exclude, filter method"""

		data = request.GET
		food_item = []

		#accessing all value of FoodItem table
		# food = FoodItem.objects.all()
		# for food_iterator in food:
		#   food_item.append({
		#       'item_name': food_iterator.item_name,
		#       'item_price': food_iterator.item_price,
		#       'item_category': food_iterator.item_category,
		#   })

		#exclude method return data excluding that item
		#food = FoodOrder.objects.exclude(pk=data.get('id'))

		#filter return only specified value
		food = FoodOrder.objects.filter(pk=data.get('id'))

		#food = FoodOrder.objects.get(item_name=data.get('id'))
		# food_item.append({
		#   'item_name': food.item_name,
		#   'item_price': food.item_price,
		#   'item_category': food.item_category,
		# })

		for food_iterator in food:
			food_item.append({
				'item_name': serializers.serialize('json', [food_iterator.item_name,]),
				'order_ammount': food_iterator.order_ammount,
			})
		return HttpResponse(json.dumps(food_item)
							,content_type='application/json')

	def post(self, request, *args, **kargs):
		"""inserting data in FoodOreder table"""

		data = request.POST

		#saving value in FoodItem Table
		# item_name_temp = data.get('item_name_temp')
		# item_price_temp = data.get('item_price_temp')
		# item_category_temp = data.get('item_category_temp')
		# item = FoodItem(item_name=item_name_temp,
		#				item_price=item_price_temp, 
		#				item_category=item_category_temp)
		# item.save()

		#Saving Value in FoodOrder table
		item_name_temp = data.get('item_name_temp')
		order_ammount_temp = data.get('order_ammount_temp')

		#create object and then put data
		# tem = FoodItem.objects.get(pk=item_name_temp)
		# order = FoodOrder(item_name=tem, order_ammount=order_ammount_temp)

		#use default table_id to put data in table
		order = FoodOrder(item_name_id=item_name_temp, order_ammount=order_ammount_temp)
		order.save()
		return HttpResponse('item value saved')

	def put(self, request, *args, **kargs):
		"""Updating price of item given item name"""

		data = QueryDict(request.body)

		#update directly FoodItem
		# item_name_temp = data.get('item_name_temp')
		# item_price_temp = data.get('item_price_temp')
		# item = FoodItem.objects.get(item_name=item_name_temp)
		# item.item_price = item_price_temp
		# item.save()

		#update FoodItem using FoodOrder object
		food_order_object = FoodOrder.objects.get(item_name = data.get('item_name_temp'))
		item_price_temp = data.get('item_price_temp')
		food_order_object.item_price = item_price_temp
		return HttpResponse('price of'
							+item_name_temp 
							+'updated using FoodOrder object')




