from django.shortcuts import render
from django.http import HttpResponse
from django.views.generic import View
from django.http import QueryDict
from parking_planner.models import Vehicle, ParkingLot
import json

# Create your views here.

#creating vehicle view
class VehicleView(View):
	"""Handle vehicle information"""

	def get(self, request, *args, **kargs):
		"""return vehicle information

		Return vehicle information using vehicle number or vecle name
		"""
		data = request.GET
		vehicle = Vehicle.objects.get(vehicle_number=data.get('vehicle_number'))
		vehicle_temp = []
		vehicle_temp.append({
			'vehicle_number': vehicle.vehicle_number,
			'vehicle_name': vehicle.vehicle_name,
			'vehicle_color': vehicle.vehicle_color,
		})
		return HttpResponse(
			json.dumps(vehicle_temp),
			content_type='application/json')

	def post(self, request, *args, **kargs):
		"""Add vehicle information"""

		data = request.POST
		vehicle_number_temp = data.get('vehicle_number')
		vehicle_name_temp = data.get('vehicle_name')
		vehicle_color_temp = data.get('vehicle_color')
		new_vehicle = Vehicle(
			vehicle_number=vehicle_number_temp, 
			vehicle_name=vehicle_name_temp, 
			vehicle_color=vehicle_color_temp)
		new_vehicle.save()
		return HttpResponse('new vehicle added')

	def put(self, request, *args, **kargs):
		"""update vehicle information"""

		data = QueryDict(request.body)
		vehicle_number_temp = data.get('vehicle_number')
		vehicle_name_temp = data.get('vehicle_name')
		vehicle = Vehicle.objects.get(vehicle_number=vehicle_number_temp)
		vehicle.vehicle_name = vehicle_name_temp
		vehicle.save()
		return HttpResponse('vehicle name of '
							+ vehicle_number_temp
							+ ' '
							+ 'updated')