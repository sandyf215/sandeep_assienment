from django.conf.urls import url
from django.views.decorators.csrf import csrf_exempt
from parking_planner.views import VehicleView

#Connect urls to different view
urlpatterns = [
	url(r'^parking_planner/vehicle/$', csrf_exempt(VehicleView.as_view())),
]