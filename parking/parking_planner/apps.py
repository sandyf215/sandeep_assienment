from django.apps import AppConfig


class ParkingPlannerConfig(AppConfig):
    name = 'parking_planner'
