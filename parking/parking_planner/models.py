from __future__ import unicode_literals

from django.db import models

# Create your models here.

#Vehicle model

class Vehicle(models.Model):
	vehicle_number = models.CharField(primary_key=True, max_length=50)
	vehicle_name = models.CharField(max_length=50)
	vehicle_color = models.CharField(max_length=50)

# Parking lot model
class ParkingLot(models.Model):
	parking_position = models.IntegerField(primary_key=True, blank=True)
	vehicle_number = models.ForeignKey(Vehicle, on_delete=models.CASCADE)
	entry_time = models.DateTimeField()
	exit_time = models.DateTimeField()



