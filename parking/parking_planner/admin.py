from django.contrib import admin
from models import Vehicle, ParkingLot

# Register your models here.
admin.site.register(Vehicle)
admin.site.register(ParkingLot)
