Project Title

Simple implementation of get, post, put method in parking.
Implementation of exclude, all, get, filter in get method 
and using post and put method in a foreign key table.

Getting Started

1.Clone the repository from link.

Prerequisites

1. Python 2.7 or above version 2
	Install using pip install python==perticular version
2. Install git client
	Install using sudo apt-get install git
3. Download Postman and install it
	Download Postman from given link https://www.getpostman.com/apps
4. Install Django version 1.9
	Install using pip install django==1.9

Running the tests

1. For Parking project-
	a. Run the postman.
	b. Run the server using python manage.py runserver
	c. Simple get, post and put method have been implemented.
2. For foreignkey project-
	a. Follow above instructions.
	b. For get method exclude, all, filter, get method have been used so 
	uncomment that part of code and run that perticuler method and comment
	rest code. give url in postman.
	c. Two ways have been implemented for post so to implement a perticular way
	just remove comment of that part.
	d. Implement put normally

Authors

Sandeep Kumar Sharma

License

This project is licensed under the MIT License - see the LICENSE.md file for details
